from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

from dock.views import simple_view
from dock.views import boat_list, boat_create, boat_detail, boat_sailor_create
from dock.views import BoatListView, BoatDetailView, BoatCreateView, BoatSailorCreateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('simple/', simple_view, name='simple-view'),
    path('cbv/simple/', TemplateView.as_view(template_name='simple.html'), name='cbv-simple-view'),

    # Views function
    path('boat/', boat_list, name='boat-list'),
    path('boat/<int:pk>/', boat_detail, name='boat-detail'),
    path('boat/create/', boat_create, name='boat-create'),
    path('boat/<int:pk>/sailor/create/', boat_sailor_create, name='boat-sailor-create'),

    # CBV
    path('cbv/boat/', BoatListView.as_view(), name='cbv-boat-list'),
    path('cbv/boat/<int:pk>/', BoatDetailView.as_view(), name='cbv-boat-detail'),
    path('cbv/boat/create/', BoatCreateView.as_view(), name='cbv-boat-create'),
    path('cbv/boat/<int:pk>/sailor/create/', BoatSailorCreateView.as_view(), name='cbv-boat-sailor-create'),
]
