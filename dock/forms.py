from django import forms
from django.core.exceptions import ValidationError

from dock.models import Boat, Sailor


class BoatForm(forms.ModelForm):

    class Meta:
        model = Boat
        fields = ['name', 'type']

    def clean_name(self):
        name = self.cleaned_data['name']
        if name == 'kikool':
            raise ValidationError('Sérieux ?')
        return name


class SailorForm(forms.ModelForm):

    class Meta:
        model = Sailor
        fields = ['first_name', 'last_name', 'alias', 'job']
