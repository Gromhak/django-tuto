from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, CreateView

from dock.models import Boat
from dock.forms import BoatForm, SailorForm


def simple_view(request):
    return HttpResponse('<h1>La vue la plus simple du monde</h1>')


def boat_list(request):
    boats = Boat.objects.all()
    return render(request, 'dock/boat/list.html', {'boats': boats})


class BoatListView(ListView):
    template_name = 'dock/boat/list.html'
    context_object_name = 'boats'
    model = Boat


def boat_create(request):
    if request.method == 'POST':
        form = BoatForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('boat-list'))
    else:
        form = BoatForm()
    return render(request, 'dock/boat/create.html', {'form': form})


class BoatCreateView(CreateView):
    template_name = 'dock/boat/create.html'
    form_class = BoatForm
    success_url = reverse_lazy('cbv-boat-list')


def boat_detail(request, pk):
    boat = Boat.objects.get(pk=pk)
    return render(request, 'dock/boat/detail.html', {'boat': boat})


class BoatDetailView(DetailView):
    template_name = 'dock/boat/detail.html'
    context_object_name = 'boat'
    model = Boat


def boat_sailor_create(request, pk):
    if request.method == 'POST':
        form = SailorForm(request.POST)
        if form.is_valid():
            boat = Boat.objects.get(pk=pk)
            form.instance.boat = boat
            form.save()
            return HttpResponseRedirect(reverse('boat-detail', kwargs={'pk': boat.pk}))
    else:
        form = SailorForm()
    return render(request, 'dock/sailor/create.html', {'form': form})


class BoatSailorCreateView(CreateView):
    template_name = 'dock/sailor/create.html'
    form_class = SailorForm

    def form_valid(self, form):
        form.instance.boat_id = self.kwargs['pk']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('cbv-boat-detail', kwargs={'pk': self.object.boat.pk})
