from django.db import models


class Boat(models.Model):

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    name = models.CharField(max_length=128)

    TYPE_PIROGUE = 'PIROGUE'
    TYPE_EGYPTIAN = 'EGYPTIAN'
    TYPE_GAILLEY = 'GAILLEY'
    TYPE_VIKING = 'VIKING'
    TYPE_GALLEON = 'GALLEON'
    TYPE_GABARRE = 'GABARRE'

    TYPE_CHOICES = (
        (TYPE_PIROGUE, 'Pirogue'),
        (TYPE_EGYPTIAN, 'Égyptien'),
        (TYPE_GAILLEY, 'Galère'),
        (TYPE_VIKING, 'Viking'),
        (TYPE_GALLEON, 'Gallion'),
        (TYPE_GABARRE, 'Gabarre'),
    )
    type = models.CharField(max_length=32, choices=TYPE_CHOICES)


class Sailor(models.Model):

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    alias = models.CharField(max_length=128, blank=True)

    JOB_CAPTAIN = 'CAPTAIN'
    JOB_SECOND = 'SECOND'
    JOB_GUNNER = 'GUNNER'
    JOB_SAILOR = 'SAILOR'

    JOB_CHOICES = (
        (JOB_CAPTAIN, 'Capitaine'),
        (JOB_SECOND, 'Second'),
        (JOB_GUNNER, 'Canonnier'),
        (JOB_SAILOR, 'Matelot')
    )
    job = models.CharField(max_length=32, choices=JOB_CHOICES)

    boat = models.ForeignKey('dock.Boat', on_delete=models.CASCADE, related_name='crew')
